<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Note;
use AppBundle\Form\NoteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FrontController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('front/index.html.twig');
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/notes", name="notes")
     */
    public function showNotesAction(Request $request)
    {
        $form = $this->createForm(new NoteType());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $note = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($note);
            $em->flush();

            $this->addFlash('success', 'Запись сохранена');
            return $this->redirectToRoute('notes');
        }

        return  $this->render('front/note.html.twig', [
            'notes' => $this->getDoctrine()->getRepository('AppBundle:Note')->findAll(),
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit-form/note-{id}", name="edit_form_note")
     */
    public function editFormNoteAction(Note $note)
    {
        $form = $this->createForm(new NoteType(), $note);

        return  $this->render('front/note_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/note-{id}", name="edit_note")
     */
    public function editNoteAction(Request $request, Note $note)
    {
        $form = $this->createForm(new NoteType(), $note);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->addFlash('success', 'Запись обновлена');
        } else {
            $this->addFlash('danger', 'Произошла ошибка');
        }

        return $this->redirectToRoute('notes');
    }

    /**
     * @Route("/remove/note-{id}", name="remove_note")
     */
    public function removeNoteAction(Note $note)
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($note);
        $manager->flush();
        $this->addFlash('success', 'Запись удалена');

        return $this->redirectToRoute('notes');
    }
}
