<?php

namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public function getName()
    {
        return 'app.extension';
    }

    public function getFilters()
    {
        return [
            'json_decode' => new \Twig_Filter_Method($this, 'jsonDecode'),
        ];
    }

    public function jsonDecode($str)
    {
        return json_decode($str);
    }
}
